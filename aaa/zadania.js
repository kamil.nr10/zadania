// Using reduce, map & filter function transform given data structures
//   Tips:
//     - different ways to call object fields  (obj.a, obj['a'])
//     - different ways to assign values to Object fields {[a]: b} (where a is a value)
//     - chaining functions (a.filter().map())

//     6.
//   - input
//     {who: 'Organization', why: 'Harmony', how: 'Human'}
//   - wyjście (lista obiektów klucz-wartość)
//     (tip: wykorzystanie Object.keys)
//     [
//       {key: 'who', value: 'Organization'},
//       {key: 'why', value: 'Harmony'},
//       {key: 'how', value: 'Human}
//     ]

// 7.
//   - input
//     [
//       {name: 'animal', age: 3},
//       {name: 'human', age: 10}
//     ]
//   - output (object {key: value})
//     {animal: 3, human: 10}

// 8.
//   - input
//     {
//       key1: 1,
//       key2: 'tar',
//       key3: [
//         {name: 'animal', age: 3},
//         {name: 'human', age: 10}
//       ]
//     }
//   - output (array of objects for each key and value)
//     [
//       {key: 'key1', value: 1},
//       {key: 'key2', value: 'tar'},
//       {
//         key: 'key3',
//         value: [
//           {name: 'animal', age: 3},
//           {name: 'human', age: 10}
//         ]
//       }
//     ]